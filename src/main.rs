use std::{fs::OpenOptions, io::Read};

fn main() {
    unsafe {
        let mut open = OpenOptions::new().create(false).read(true).write(false).open("break:").unwrap();
        let mut buffer_orig = String::from("ffffffffffffffffffffbbbbbbbbbbbbbbbbbbbb");
        let buffer = buffer_orig.as_mut_str();
        let buffer_to_write_to= buffer[20..].as_bytes_mut();
        
        println!("Buffer begins: {}", buffer_to_write_to[0] as char);

        open.read(buffer_to_write_to).unwrap();

        println!("Found data: {}", buffer_to_write_to[0] as char);
    }
}
